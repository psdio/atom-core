(function() {
    var el=document.createElement('div'),
    b=document.getElementsByTagName('body')[0];
    otherlib=false,
    msg='';
    el.style.position='fixed';
    el.style.height='40px';
    el.style.width='220px';
    el.style.marginLeft='-110px';
    el.style.top='0';
    el.style.left='50%';
    el.style.padding='5px 10px';
    el.style.zIndex = 9999;
    el.style.fontSize='12px';
    el.style.color='#222';
    el.style.backgroundColor='#fff';
    el.style.boxShadow= "0px 5px 5px rgba(0,0,0,0.2)";

    // more or less stolen form jquery core and adapted by paul irish
    function getScript(url,success){
            var script=document.createElement('script');
            script.src=url;
            var head=document.getElementsByTagName('head')[0],
            done=false;
        // Attach handlers for all browsers
        script.onload=script.onreadystatechange = function(){
            if ( !done && (!this.readyState
                || this.readyState == 'loaded'
                || this.readyState == 'complete') )
            {
                done=true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            }};
        head.appendChild(script);
    }

    window.showScriptDialog = showDialog;
    function showDialog() {
        el.innerHTML="Paste the url and press enter:<br/><input id='script-src'>";
        b.appendChild(el);

        var input = document.getElementById("script-src");
        input.style.width='215px';
        input.addEventListener("keyup", function(event) {
            if (event.keyCode === 13) {
                var url = input.value;
                
                getScript(url ,function() {
                    msg='Script '+url+" loaded";
                    return showMsg();
                });
            }
        });
    }

    var url = "https://code.jquery.com/jquery-2.1.4.min.js";
    getScript(url, function() {
        msg='Script '+url+" loaded";
        return showMsg();
    });
    // showScriptDialog(); 

    function showMsg() {
        el.innerHTML=msg;
        b.appendChild(el);
        window.setTimeout(function() {
            if (typeof jQuery=='undefined') {
                b.removeChild(el);
            } else {
                jQuery(el).fadeOut('slow',function() {
                    jQuery(this).remove();
                });
                if (otherlib) {
                    $jq=jQuery.noConflict();
                }
            }
        } ,2500);
    }
})();