var indexCrawler = require("./core/index-crawler");
var detailCrawler = require("./core/detail-crawler");
var validator = require("validator");
var commandLineArgs = require("command-line-args");
var sprintf = require("sprintf");
var Log = require("./core/utils/log");
var Toolkit = require("./core/utils/toolkit");
var url = require('url');
var _ = require("underscore");

var cli = commandLineArgs([
    { name: "host", type: String, defaultOption: true },
    { name: "advanced", type: Boolean, defaultValue: false },
    { name: "regex", type: String, defaultOption: true },
    { name: "sample", type: Number, defaultValue: 30 },
    { name: "name", type: String },
    { name: "file", type: String },
]);
var options = cli.parse();
if (options.host === undefined && options.file === undefined) {
    Log.info("Please specific the host to parse [ usage: --host=http://tvmost.com.hk ]");
    process.exit();
}

if (options.regex === undefined && options.file === undefined) {
    Log.info("Please specific the regex for the url of detail page [ usage: --regex= ]");
    process.exit();
}

var advancedMode = !!options.advanced;
var endpoint = Toolkit.resolveUrl(options.host);
var regexStr = options.regex;
var regex = new RegExp(regexStr);

if (!endpoint && options.file === undefined) {
    Log.error("Invalid endpoint");
    process.exit();
}

if (!regex) {
    Log.error("Invalid regex");
    process.exit();
}

var callback = function(urls) {
    detailCrawler.captureSiteFeatures(_.clone(urls), function(commonPaths) {
        var parts = url.parse(urls[0]);
        var saveOptions = {
            domain: parts.hostname,
            endpoint: urls[0],
            regex: regexStr,
            commonPaths: commonPaths
        }
        if (options.name !== undefined) { saveOptions.name = options.name; }
        indexCrawler.saveFeature(endpoint, saveOptions);
    });
}

if (options.file !== undefined) {
    var urls = [];
    require('readline')
        .createInterface({ input: require('fs').createReadStream(options.file) })
        .on('line', function (line) { urls.push(line); })
        .on('close', function() {
            urls = urls.slice(0, options.sample)
            callback(urls);
        })
} else {
  indexCrawler.getPossibleUrls(endpoint, {
    endpoint: endpoint,
    advanced: advancedMode,
    complete: function(urls) {
        urls = _.filter(urls, function(url) { 
            return validator.isURL(url) && regex.test(url); 
        });
        urls = urls.slice(0, options.sample)
        console.log(urls)

        var parts = url.parse(endpoint);
        detailCrawler.captureSiteFeatures(urls, function(commonPaths) {
            indexCrawler.saveFeature(endpoint, {
                domain: parts.hostname,
                endpoint: endpoint,
                regex: regexStr,
                commonPaths: commonPaths
            });
        });
    }
  });
}