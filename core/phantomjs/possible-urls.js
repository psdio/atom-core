var PhantomBase = require("./base");
var page = require('webpage').create();
var args = require('system').args;
var endpoint = args[1];

PhantomBase.setup(page);
page.open(endpoint, function(status) {
    var PossibleUrls = require("../modules/possible-urls");
    page.includeJs('http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js');

    var fnStr = 'function(){ var f = '+PossibleUrls.Universal.getUrls.toString()+'; return f($); } ';
    PhantomBase.output(page.evaluateJavaScript(fnStr));
    phantom.exit();
});