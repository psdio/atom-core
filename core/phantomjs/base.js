module.exports = {

    userAgent: "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.3 (KHTML, like Gecko) Chrome/6.0.472.63 Safari/534.3",
    setup: function(page) {
        page.settings.userAgent = this.userAgent;
        page.onError = this.handlers.onError;
        return page;
    },

    output: function(output) {
        console.log(output);
    },

    handlers: {

        onError: function(msg, trace) {
            var msgStack = ['ERROR: ' + msg];
            if (trace && trace.length) {
                msgStack.push('TRACE:');
                trace.forEach(function(t) {
                    msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
                });
            }
            // uncomment to log into the console 
            // console.error(msgStack.join('\n'));
        }
    }
};