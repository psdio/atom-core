var Toolkit = require("../utils/toolkit");
var UrlParse = require("./url-parse");

module.exports = {

    /**
     * Get all possible urls from given document
     * @param  {Object} $             Pass in a cheer.io document or jQuery object
     * @return {Array}
     */
    all: function($, options) {
        if (typeof jQuery !== "undefined") {
            $ = jQuery;
        }
        var possibleUrls = [];
        var urls = JSON.parse(this.Universal.getUrls($));
        for (var i = 0; i < urls.length; i++) {
            var link = this.Universal.filter(urls[i], options);
            if (link) {
                possibleUrls.push(link);
            }
        };
        return possibleUrls;
    },

    Universal: {

        /**
         * Get urls from DOM
         * @param  {Object} $         Pass in a cheer.io document or jQuery object
         * @return {String}           Array of urls in JSON format
         */
        getUrls: function($) {
            var possibleUrls = [];
            $("a").each(function() {
                var link = $(this).attr("href");
                if (link) {
                    possibleUrls.push(link);
                }
            });
            return JSON.stringify(possibleUrls);
        },

        /**
         * Filter out url with no path for possible urls
         * @param  {String} link 
         * @return {String}
         */
        filter: function(link, options) {
            link = Toolkit.resolveUrl(link, UrlParse(options.endpoint));
            if (link) {
                var linkParts = UrlParse(link);
                if (linkParts.path.length > 0 && linkParts.path !== "/") {
                    return link;
                }
            }
            return false;
        },
    }
}