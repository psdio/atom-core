function DomNode(path) {
    this.path = path;
    this.parts = path.split(">");
};

function getCommentParts(array1, array2) {
    var paths = [];
    for (var i = 0; i < array1.length; i++) {
        if (array1[i] === array2[i]) {
            paths.push(array1[i]);
        } else {
            break;
        }
    };
    return paths;
}

DomNode.prototype = {

    eq: function(node) {
        return this.path === node.path;
    },

    getParentNode: function() {
        return new DomNode(this.getParentPath());
    },

    getParentPath: function() {
        return this.parts.slice(0, this.parts.length - 1).join(">");
    },

    getDistanceToNode: function(node) {
        if (!(node instanceof DomNode)) {
            return -1;
        }

        return Math.abs(this.parts.length - node.parts.length);
    },

    isUnderTheNode: function(node) {
        var myParts = this.parts;
        var parts = node.parts;
        for (var i = 0; i < parts.length; i++) {
            if (parts[i] !== myParts[i]) {
                return false;
            }
        };

        if (parts.length === myParts.length) {
            return false;
        }

        return true;
    },

    getCommonNode: function(node) {
        var array1 = this.parts;
        var array2 = node.parts;

        var result;
        if (array1.length > array2.length) {
            result = getCommentParts(array1, array2);
        } else {
            result = getCommentParts(array2, array1);
        }
        return new DomNode(result.join(">"));
    }
};

module.exports = DomNode;