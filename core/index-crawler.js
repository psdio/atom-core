var unirest = require("unirest");
var cheerio = require("cheerio");
var sprintf = require("sprintf");
var moment = require("moment");
var url     = require('url');
var childProcess = require('child_process')

var Log = require("./utils/log");
var Toolkit = require("./utils/toolkit");
var _ = require("underscore");

/* Modules */
var PossibleUrls = require("./modules/possible-urls");

module.exports = {
    getPossibleUrls: function(endpoint, options) {
        if (options === undefined) { options = {}; }

        var start = _.now() ;
        var onError = function(error) {
            Log.error("Unable to get to endpoint. Error: %s", JSON.Stringify(response.error));
            if (optinos.cli === true) {
                process.exit();
            }
        }

        Log.info("Started to crawl %s", endpoint);
        if (options.advanced === true) {
            childProcess.execFile("phantomjs", [ "core/phantomjs/possible-urls.js", endpoint ], function(err, stdout, stderr) {
                Log.info("Web page downloaded. Time taken: %s ms", (_.now() - start));
            });
            childProcess.execFile("phantomjs", [ "core/phantomjs/possible-urls.js", endpoint ], function(err, stdout, stderr) {
                Log.info("Web page downloaded. Time taken: %s ms", (_.now() - start));
            });
            childProcess.execFile("phantomjs", [ "core/phantomjs/possible-urls.js", endpoint ], function(err, stdout, stderr) {
                Log.info("Web page downloaded. Time taken: %s ms", (_.now() - start));
            });

            // childProcess.execFile("phantomjs", [ "core/phantomjs/possible-urls.js", endpoint ], function(err, stdout, stderr) {
            //     Log.info("Web page downloaded. Time taken: %s ms", (_.now() - start));
            //     if (err) {
            //         onError(err);
            //     }

            //     var possibleUrls = err ? [] : JSON.parse(stdout);
            //     if (options.complete) {
            //         options.complete(_.uniq(possibleUrls));
            //     }
            // });
        } else {
            unirest.get(endpoint).end(function (response) {
                Log.info("Web page downloaded. Time taken: %s ms", (_.now() - start));
                if (response.error) {
                    onError(response.error);
                }

                // Loading the HTML in
                var $ = cheerio.load(response.body, {
                    normalizeWhitespace: true,
                    decodeEntities: true
                });

                // Looking for detail page url
                var possibleUrls = PossibleUrls.all($, options);
                if (options.complete) {
                    options.complete(_.uniq(possibleUrls));
                }
            });
        }
    },

    saveFeature: function(endpoint, options) {
        var data = options;
        var dir = sprintf("./results/%s", options.name ? options.name : options.domain);
        Toolkit.makeDirectoryByPath(dir);

        var outputFilename = dir+"/feature.json";
        require('fs').writeFile(outputFilename, JSON.stringify(data, null, 4), function(err) {
            if (err) {
                console.log("ERROR");
                console.log(err);
                // Log.error(err);
            } else {
                Log.info("Site feature saved [%s]", JSON.stringify(data, null, 4));
                console.log(outputFilename);
            }
        }); 
    }
}