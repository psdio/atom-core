var unirest = require("unirest");
var cheerio = require("cheerio");
var sprintf = require("sprintf");
var Q = require("q");
var _ = require("underscore");
var tr = require('transliteration');

var Log = require("./utils/log");
var Toolkit = require("./utils/toolkit");
var HtmlProcessor = require("./utils/html-processor")

var DomNode = require("./classes/DomNode");

var sortKeysByValue = function(obj) {
    var arr = [];
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push({
                'key': prop,
                'value': obj[prop]
            });
        }
    }
    arr.sort(function(a, b) { return b.value - a.value; });
    return arr;
}

function getCommonPaths(validPaths) {
    var domNodes = _.map(_.uniq(validPaths), function(path) { 
        return new DomNode(path);
    });

    // Getting the common root for paths
    var commonPaths = {};
    _.each(domNodes, function(domNode) {
        if (Object.keys(commonPaths).length > 0) {
            // Common paths exists, check if the node share the same common path
            var found = false;
            _.each(commonPaths, function(commonPath, key) {
                if (found === true) {
                    return ;
                }
                var originalCommonNode = commonPath;
                var tempCommonNode = domNode.getCommonNode(originalCommonNode);
                var commonNodeDistance = originalCommonNode.getDistanceToNode(tempCommonNode);

                if (originalCommonNode.eq(tempCommonNode)) {
                    // Common node is same as the stored node, nothing to do
                    found = true;
                } else if (tempCommonNode.isUnderTheNode(originalCommonNode)) {
                    // New common node is under the stored node, check if we need to replace it

                    // The new common node is too far from current node, consider to store the content separately
                    if (tempCommonNode.getDistanceToNode(originalCommonNode) > 3) {
                        found = false;
                    } else {
                    // The stored path is inside the new common node, replace it
                        delete commonPaths[originalCommonNode.path];
                        commonPaths[tempCommonNode.path] = tempCommonNode;
                        found = true;
                    }
                } else if (originalCommonNode.isUnderTheNode(tempCommonNode) && commonNodeDistance >= 0 && commonNodeDistance < 3) {
                    delete commonPaths[originalCommonNode.path];
                    commonPaths[tempCommonNode.path] = tempCommonNode;
                    found = true;
                } else {
                    found = false;
                }
            });

            if (found === false) {
                var parentNode = domNode.getParentNode();
                commonPaths[parentNode.path] = parentNode;
            }

        } else {
            // Common paths not exist, put the parent in anyway
            var parentNode = domNode.getParentNode();
            commonPaths[parentNode.path] = parentNode;
        }
    });

    var result = {};
    for (var key in commonPaths) {
        var node = commonPaths[key];
        if (node.parts.length <= 2) {
            continue;
        }

        var parts = node.parts;
        for (var index in parts) {
            parts[index] = parts[index].match(/([a-zA-Z]+[#]?[\.]?[a-zA-Z\-\_]+)(\[\\d\])?/)[0];
        }

        node.parts = parts;
        node.path = parts.join(">");
        result[node.path] = node;
    }
    
    return Object.keys(result);
}

var ignoredTags = [ "script", "style", "noscript", "iframe", "br" ];
var domRecords = {};

function _writeContent(folder, filename, content) {
    Toolkit.makeDirectoryByPath(folder);
    var outputFilename = folder+"/"+filename;
    require('fs').appendFile(outputFilename, content, function(err) {
        if (err) {
            console.log("ERROR");
            console.log(err);
            // Log.error(err);
        } else {
            Log.debug("File " + outputFilename+" updated");
        }
    }); 
}

var defaultOptions = {
    getContent: {

    }
};

module.exports = {
    getContent: function(url, options) {
        var deferred = Q.defer();
        var parts = require('url').parse(url);
        var hostname = parts.hostname;
        var hostPath = tr(parts.path.replace(/\//g, ""));
        var filename = sprintf("./results/%s/feature.json", options.name ? options.name : hostname);
        var config = null;

        if (options === undefined) {
            options = {};
        }

        options.urlRoot = sprintf("%s//%s", parts.protocol, hostname);
        options.hostPath = hostPath;

        try {
            config = require('jsonfile').readFileSync(filename);
        } catch (e) {
            if ( e.code === 'ENOENT' ) {
                Log.error("Feature for site %s not found, please run feature scan first", hostname);
                if (options.cli === true) {
                    process.exit();
                }
            }
        }
        
        if (!config) {
            if (options.error) { options.error(); }
            if (options.complete) { options.complete(); }
            deferred.reject();
            return deferred.promise;
        }

        var result = {};
        var start = _.now();
        Log.info("Going to get the content from url %s", url);
        unirest.get(url).end(function (response) {
            Log.info("Webpage downloaded in %s ms. URL: %s", (_.now() - start), url);

            // Error
            if (response.error) {
                console.log(response.error);
                deferred.reject();
                Log.error("Unable to get to webpage %s. Error: %s", url, JSON.stringify(response.error));
            } else {
                var dir = sprintf("./results/%s/output", (options.name ? options.name : hostname));
                var filename = sprintf("%s-%s.html", hostPath, (new Date().getTime() + ''));
                // Loading the HTML in
                var $ = cheerio.load(response.body, {
                    normalizeWhitespace: true,
                    decodeEntities: true
                });
                var $body = $("body");
                for (var i = 0; i < config.commonPaths.length; i++) {
                    var path = config.commonPaths[i];
                    var $node = $body.find(path);
                    if ($node && $node.html() && $node.html().length > 0) {
                        var content = HtmlProcessor.clearHtml($body.find(path).html(), options);
                        _writeContent(dir, filename, content);
                        result[path] = content;
                    }
                };

                if (options.success) {
                    options.success(result);
                }
                deferred.resolve();
            }

            if (options.complete) {
                options.complete();
            }
        });
        return deferred.promise;
    },

    captureSiteFeatures: function(urls, completion) {
        domRecords = {};
        var self = this;
        var elementThreshold = 0.6;
        var batchSize = 10;
        var total = urls.length;
        var next = function() {
            batchUrls = urls.splice(0, batchSize);
            var tasks = _.map(batchUrls, function(url) {
                return self.extractPageFeature(url);
            });
            Q.all(tasks).fin(function(results) {
                console.log("Numer of urls captured: "+(total-urls.length))
                if (urls.length > 0) {
                    next();
                } else {
                    onSitesScanned();
                }
            });
        }
        var onSitesScanned = function() {
            Log.info("Finished processing on the websites, now calculate similar elements");

            // Loop through the stat and filter out elements over the threshold
            var validPaths = [];
            var sortedRecords = sortKeysByValue(domRecords);
            for (var i = 0; i < sortedRecords.length; i++) {
                if (sortedRecords[i].value / total < elementThreshold) {
                    validPaths.push(sortedRecords[i].key);
                }
            };

            // Keep the ordering from DOM original structure
            var contentPaths = _.filter(_.map(domRecords, function(value, key) {
                return _.indexOf(validPaths, key) >= 0 ? key : false;
            }));

            // Filter out invalid paths and strict DOM id
            var commonPaths = getCommonPaths(contentPaths);

            Log.info("Got a list of common roots of the website for parsing");
            Log.debug(commonPaths);
            if (completion) {
                completion(commonPaths);
            }
        }

        console.log("Tota: "+total)
        next();
    },

    extractPageFeature: function(url) {
        var deferred = Q.defer();
        var start = _.now();

        var getNodeIdentifier = function($node) {
            var nodeIdentifier = $node.get(0).tagName;
            var nodeClass = $node.attr("class") ? $node.attr("class").trim() : undefined;

            if ($node.attr("id") !== undefined) {
                nodeIdentifier = sprintf("%s#%s", nodeIdentifier, $node.attr("id"));
            } else if (nodeClass !== undefined && nodeClass.length > 0) {
                var classes = _.filter($node.attr("class").split(" "));
                nodeIdentifier = sprintf("%s.%s", nodeIdentifier, classes[0]); // FIXME: For simpilcity I will just take the first class here. Probably cause some issues if the classes are not well used
            } else {
                nodeIdentifier = sprintf("%s[%s]", nodeIdentifier, $node.index());
            }
            return nodeIdentifier;
        }

        Log.info("Going to scan detail page with url %s", url);
        unirest.get(url).end(function (response) {

            Log.info("Webpage downloaded in %s ms. URL: %s", (_.now() - start), url);

            // Error
            if (response.error) {
                    console.log(response.error);
                    Log.error("Unable to get to endpoint %s. Error: %s", url, JSON.stringify(response.error));
                    // deferred.reject(url);
                    // return ;
                } else {

                // Loading the HTML in
                var $ = cheerio.load(response.body, {
                    normalizeWhitespace: true,
                    decodeEntities: true
                });
                var $body = $("body");

                var scanDomTree = function($element, depth) {
                    if ($element.get(0) === undefined) {
                        // Bad element
                        return ;
                    }

                    var elementName = getNodeIdentifier($element);
                    if (depth === undefined) {
                        depth = [];
                    } else {
                        var depth = depth.slice(0);
                        depth.push(elementName);
                    }

                    var elementIndex = {};
                    $element.children().each(function() {
                        var $node = $(this);

                        var nodeIdentifier = getNodeIdentifier($node);
                        if (_.indexOf(ignoredTags, $node.get(0).tagName) < 0) {
                            var treeAddress;
                            if (depth.length === 0) {
                                treeAddress = nodeIdentifier;
                            } else {
                                treeAddress = sprintf("%s>%s", depth.join(">"), nodeIdentifier);
                            }

                            // Building own dom tree
                            // domMap[treeAddress] = {
                            //     parent: depth.join(">"),
                            //     depth: depth.length + 1,
                            // };

                            // Setting the statistic
                            if (domRecords[treeAddress] === undefined) {
                                domRecords[treeAddress] = 0;
                            }
                            domRecords[treeAddress]++;

                            // Scan the next level
                            scanDomTree($node, depth);
                        }
                    });
                }
                
                // var domMap = {};
                scanDomTree($body);
            }

            // Looking for similar items
            
            Log.debug("DONE extract");
            deferred.resolve();
        });

        return deferred.promise;
    }
};