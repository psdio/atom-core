var sanitizeHtml = require('sanitize-html');
var cheerio = require("cheerio");
var _ = require("underscore");
var url = require('url');

module.exports = {
    clearHtml: function(html, options) {

        // Fix image urls
        var $ = cheerio.load(html, {
            normalizeWhitespace: true,
            decodeEntities: true
        });
        $("img").each(function() {
            var src = $(this).attr("src");

            // Absoulte path, update it with the domain
            if (_.indexOf(src, "/") === 0) {
                $(this).attr("src", url.resolve(options.urlRoot, src));
            }
        });


            
            
            "span[style]", "img[width|height|alt|src]", "iframe[src]"

        return sanitizeHtml($.html(), {
            allowedTags: [ "h1", "h2", "h3", "h4", "h5", 
                           "b", "strong", "i", "a",
                           "ul", "ol", "li", "p",
                           "blockquote", "img",
                           "span" ],
            allowedAttributes: {
                "a": ["href", "title"],
                "img": [ "src", "alt" ]
            }
        });
    }
};