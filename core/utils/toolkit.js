var moment  = require("moment");
var sprintf = require("sprintf");
var _       = require("underscore");
var is      = require("is_js");
var fs      = require('fs');
var q       = require('q');
var urlParse = require('../modules/url-parse');

module.exports = {
    Array: {
        getValueByPath: function(array, path, defaultValue) {
            var props = path.split('.'), item = array;
            for (var i = 0; i < props.length; i++) {
                item = item[props[i]];

                if (item === undefined && defaultValue !== undefined) {
                    return defaultValue;
                } else if (item === undefined) {
                    return undefined;
                }
            }

            return item;
        },
        setValueByPath: function(object, path, value) {
            var props = path.split('.'), item = object;
            for (var i = 0; i < props.length; i++) {
                if (item[props[i]] === undefined) { item[props[i]] = {} };
                if (i == props.length - 1) {
                    item[props[i]] = value;
                } else {
                    item = item[props[i]];
                }
            }
            return object;
        }
    },
    makeDirectory: function (dir) {
        try {
            fs.mkdirSync(dir);
        } catch(e) {
            if ( e.code != 'EEXIST' ) throw e;
        }
    },
    makeDirectoryByPath: function (dirpath) {
        var path    = require('path');
        var parts = dirpath.split(path.sep);
        for( var i = 1; i <= parts.length; i++ ) {
            this.makeDirectory( path.join.apply(null, parts.slice(0, i)) );
        }
    },
    resolveUrl: function(link, domainParts) {
        if (is.not.existy(link) || is.empty(link)) {
            return false;
        }

        if (link.indexOf("//") === 0) { // Url without protocol
            link = sprintf("%s:%s", domainParts.protocol, link);
        } else if (link.indexOf("/") === 0) { // Url in absolute path
            var endpointRoot = sprintf("%s://%s%s", domainParts.protocol, domainParts.host, (domainParts.port.length > 0 ? ":"+domainParts.port : ""));
            link = sprintf("%s%s", endpointRoot, link);
        } else if (link.indexOf("javascript") === 0 || link.indexOf("#") === 0 || _.isEmpty(link)) {
            // Ignore javascript, hash link and empty link
            return false;
        }

        // Fix for url without protocol
        var linkParts = urlParse(link);
        if (_.indexOf([ "http", "https" ], linkParts.protocol) < 0) {
            link = "http://"+link;
            linkParts = urlParse(link);
        }

        if (domainParts === undefined) {
            domainParts = linkParts;
        }

        // Skip urls in external domain
        if (is.not.url(link) ||                      // Is valid URL
            linkParts.host !== domainParts.host) {  // With in domain
            return false;
        }

        // Just ignore the query param and hash
        // FIXME: Probably cause some issues for some websites
        link = sprintf("%s://%s%s%s", linkParts.protocol, linkParts.host, linkParts.port ? ":"+linkParts.port : "", linkParts.path); 
        return link;
    },
    q_batch: function($q, tasks, batch_size) {
      //Finished?
      if (!tasks || tasks.length == 0) {
          $q.resolve(tasks);
          console.log("Finished");
          return;
      }
      //Call the function batch_size times to get the promises
      var promise_arr = [];
      for (var i = 0; (i < batch_size) && (i < tasks.length); i++) {
          try {
              var task = tasks[i];
              console.log("Calling " + task.fn.name + " with " + task.arg);
              promise_arr.push(task.fn.call(this, task.arg));
          } catch (e) {
              $q.reject("Batch failed. " + e);
          }
      }
      //Execute the batch_size, then go again
      var promise_batch = q.all(promise_arr);
      promise_batch.then(function(results) {
          try {
              //Remove the first n funcs and go again.
              tasks.splice(0, batch_size);
              q_batch($q, tasks, batch_size);
          } catch (err) {
              console.error(err.stack ? err.stack : err);
          }
      }, function(err) {
          $q.reject("Batch failed. " + err);
      });
      return $q.promise;
    }
}