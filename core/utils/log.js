var moment = require("moment");
var sprintf = require("sprintf");
var ccolor = require('cli-color');
var _ = require("underscore");

var watchers = {};

module.exports = {

    addWatcher: function(request) {
        if (request.logWatcher) {
            watchers[request.id] = request.logWatcher;
        }
    },

    unregisterWatcher: function(request) {
        delete watchers[request.id];
    },

    info: function(msg) {
        this._print.apply(this, _.union(["I"], _.values(arguments)));
    },

    error: function(msg) {
        this._print.apply(this, _.union(["E"], _.values(arguments)));
    },

    debug: function(msg) {
        if (msg instanceof Object || msg instanceof Array) {
            console.log(ccolor.green("============= DEBUG ============="));
            console.log(msg);
            console.log(ccolor.green("=========== DEBUG END ==========="));
        } else {
            this._print.apply(this, _.union(["D"], _.values(arguments)));
        }
    },

    _print: function(type, msg) {
        var time = moment().format('DD-MM-YYYY h:mm:ss');
        var args = _.values(arguments).slice(1, arguments.length);
        if (args.length > 0) {
            msg = sprintf.apply(this, _.union([msg], args));
        }
        var output = sprintf("[%s][%s] %s", type, time, msg);
        if (type === "E") {
            output = ccolor.red(output);
        } else if (type === "D") {
            output = ccolor.green(output);
        }
        
        console.log(output);
        if (Object.keys(watchers).length > 0) {
            for (var reqId in watchers) {
                watchers[reqId](type, time, msg);
            }
        }
    }
};