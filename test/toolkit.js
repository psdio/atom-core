var assert = require("assert");
var Toolkit = require("../core/utils/toolkit");
var UrlParse = require("../core/modules/url-parse");

describe('Utils', function() {
    describe('Toolkit', function() {
        describe('Array', function() {
            describe('getValueByPath', function () {
                var data = {
                    "a": {
                        "a1": {
                            "a2": "A Value"
                        }
                    }, 
                    "b": {
                        "b1": {

                        },
                        "bb1": {
                            "b2": "B Value"
                        }
                    }
                };

                it('Get value by path', function () {
                    assert.equal("A Value", Toolkit.Array.getValueByPath(data, "a.a1.a2"));
                });
                it('Get value by path but key not set', function () {
                    assert.equal(undefined, Toolkit.Array.getValueByPath(data, "a.a1.a3"));
                });
                it('Get value by path but key not set in multiple level', function () {
                    assert.equal(undefined, Toolkit.Array.getValueByPath(data, "a.c3.b5.w8.q1"));
                });
                it('Get value by path but key not set and return default value', function () {
                    assert.equal("DEFAULT VALUE", Toolkit.Array.getValueByPath(data, "a.a1.a3", "DEFAULT VALUE"));
                });
                it('Get value by path but key not set in multiple level and return default value', function () {
                    assert.equal("DEFAULT VALUE", Toolkit.Array.getValueByPath(data, "a.c3.b5.w8.q1", "DEFAULT VALUE"));
                });
            });

            describe("setValueByPath", function () {
                it('Simply set value', function () {
                    var object = {};
                    Toolkit.Array.setValueByPath(object, "a", "A Value");
                    assert.equal("A Value", object["a"]);
                });
                it('Set value by path', function () {
                    var object = {};
                    Toolkit.Array.setValueByPath(object, "a.a1.a2", "A Value");
                    assert.equal("A Value", object["a"]["a1"]["a2"]);
                });
                it("Set value by path with existing properties", function () {
                    var object = {
                        "b": {
                            "b1": {
                                "b2": "B Value"
                            }
                        }
                    };
                    Toolkit.Array.setValueByPath(object, "a.a1.a2", "A Value");
                    assert.equal("A Value", object["a"]["a1"]["a2"]);
                    assert.equal("B Value", object["b"]["b1"]["b2"]);
                });
            });
        });

        describe('resolveUrl', function () {
            var siteRoot = "http://www.atom-crawler.com.hk";
            var resultUrl = "http://www.atom-crawler.com.hk/help";
            it('Fix url start with //', function () {
                var domainParts = UrlParse(siteRoot);
                assert.equal(resultUrl, Toolkit.resolveUrl("//www.atom-crawler.com.hk/help", domainParts));
            });
            it('Fix url without protocol', function () {
                var domainParts = UrlParse(siteRoot);
                assert.equal(resultUrl, Toolkit.resolveUrl("www.atom-crawler.com.hk/help", domainParts));
            });
            it('Fix url with absolute path', function () {
                var domainParts = UrlParse(siteRoot);
                assert.equal(resultUrl, Toolkit.resolveUrl("/help", domainParts));
            });
            it('Filter out Javascript', function () {
                var domainParts = UrlParse(siteRoot);
                assert.equal(false, Toolkit.resolveUrl("javascript: alert('help');", domainParts));
            });
            it('Filter out hash', function () {
                var domainParts = UrlParse(siteRoot);
                assert.equal(false, Toolkit.resolveUrl("#help", domainParts));
            });
            it('Filter out empty url', function () {
                var domainParts = UrlParse(siteRoot);
                assert.equal(false, Toolkit.resolveUrl("", domainParts));
            });
            it('Filter url in diffent domain', function () {
                var domainParts = UrlParse(siteRoot);
                assert.equal(false, Toolkit.resolveUrl("http://www.atom-invisible.com.hk", domainParts));
            });
        });
    });
});