var assert = require("assert");
var UrlParse = require("../core/modules/url-parse");
var PossibleUrls = require("../core/modules/possible-urls");

describe('Modules', function() {
    describe('possible-urls', function() {
        describe('filter', function() {
            it("Filter url without path", function () {
                assert.equal(false, PossibleUrls.Universal.filter("http://www.atom-crawler.com.hk/"));
            });
        });
    });
});