var jade = require("jade");
var express = require("express");
var sprintf = require("sprintf");
var _ = require("underscore");
var is = require("is_js");
var url = require("url");

var Log = require("./core/utils/log");
var Toolkit = require("./core/utils/toolkit");

var app = express();

var indexCrawler = require("./core/index-crawler");
var detailCrawler = require("./core/detail-crawler");

var sysConfig = require('jsonfile').readFileSync("./config/default.json");

app.set("view engine", "jade");

app.use(express.static("public"));
app.use(require('body-parser').json());
app.use(require('connect-requestid'))

// Post processer to unregister log watcher
app.use(function(req, res, next){
    res.on('finish', function(){
        Log.unregisterWatcher(req);
    });
    next();
});

// Override default render method to inject configurations
app.use(function(req, res, next) {
    var _render = res.render;
    res.render = function(view, options, fn) {
        if (options === undefined) {
            options = {};
        }
        options["config"] = sysConfig["web"];
        _render.call(this, view, options, fn);
    }
    next();
});

/* Web interface */

app.get("/", function (req, res) {
    res.render("index", {
        navigate: "sites.index"
    });
});

app.get("/scan", function (req, res) {
    var sites = [];

    // options is optional
    var glob = require("glob");
    glob("./results/**/feature.json", {}, function (er, files) {
        for (var i = 0; i < files.length; i++) {
            var site = {};
            site.domain = files[i].match(/\.\/results\/(.*)\/feature\.json/)[1];
            var config = require("jsonfile").readFileSync(files[i]);
            sites.push(_.extend(site, config));
        };

        res.render("scan", {
            navigate: "scan.index",
            sites: sites
        });
    });
});

app.get("/crawl", function(req, res) {
    res.render("crawl", {
        navigate: "crawl.index"
    });
});

app.get("/debug", function(req, res) {
    res.render("debug", {
        navigate: "service.debug"
    });
});

/* API */

app.get("/api/url-debug", function(req, res, next) {
    var host = req.query["host"];
    host = Toolkit.resolveUrl(host);

    try {
        var regex = new RegExp(req.query["regex"] ? req.query["regex"] : ".*");
    } catch (e) {
        res.status(400).json({ 
            success: false,
            message: "invalid regex"
        });
        return ;
    }

    if (is.not.existy(host) || is.empty(host) || !host) {
        res.status(400).json({ 
            success: false,
            message: "invalid host"
        });
        return ;
    }

    indexCrawler.getPossibleUrls(host, function(urls) {
        urls = _.filter(urls, function(url) { 
            return is.url(url) && regex.test(url); 
        });
        res.json({ 
            success: true,
            data: {
                urls: urls
            }
        });
    });
});

app.post("/api/scan-feature", function(req, res, next) {
    var endpoint = req.body["host"];
    endpoint = Toolkit.resolveUrl(endpoint);

    try {
        var regexStr = req.body["regex"] ? req.body["regex"] : ".*"
        var regex = new RegExp(regexStr);
    } catch (e) {
        res.status(400).json({ 
            success: false,
            message: "invalid regex"
        });
        return ;
    }

    if (is.not.existy(endpoint) || is.empty(endpoint) || !endpoint) {
        res.status(400).json({ 
            success: false,
            message: "invalid host"
        });
        return ;
    }

    indexCrawler.getPossibleUrls(endpoint, function(urls) {
        urls = _.filter(urls, function(url) { 
            return is.url(url) && regex.test(url); 
        });

        var parts = url.parse(endpoint);
        detailCrawler.captureSiteFeatures(urls, function(commonPaths) {
            var settings = {
                domain: parts.hostname,
                endpoint: endpoint,
                regex: regexStr,
                commonPaths: commonPaths
            };
            indexCrawler.saveFeature(endpoint, settings);

            res.json({ 
                success: true,
                data: settings
            });
        });
    });
});

app.post("/api/crawl", function(req, res, next) {
    var endpoint = req.body["endpoint"];
    endpoint = Toolkit.resolveUrl(endpoint);

    var logs = [];
    req.logWatcher = function(type, time, msg) {
        logs.push(sprintf("[%s] %s", time, msg));
    }
    Log.addWatcher(req);

    detailCrawler.getContent(endpoint, {
        success: function(result) {
            res.json({ 
                success: true,
                data: {
                    logs: logs,
                    result: result
                }
            });
        },
        error: function() {
            res.status(400).json({ 
                success: false,
                data: {
                    logs: logs
                }
            });
        }
    });
});


var server = app.listen(3387, function () {
    var host = server.address().address;
    var port = server.address().port;
    Log.info("Web interface started listening at http://%s:%s", host, port);
});