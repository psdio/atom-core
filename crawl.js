var commandLineArgs = require("command-line-args");
var Log = require("./core/utils/log");
var Toolkit = require("./core/utils/toolkit");
var _ = require("underscore"); 
var Q = require("q");

var indexCrawler = require("./core/index-crawler");
var detailCrawler = require("./core/detail-crawler");

var cli = commandLineArgs([
    { name: "src", type: String, multiple: true, defaultOption: true },
    { name: "file", type: String },
    { name: "batch-size", type: Number, defaultValue: 10 }
]);
var options = cli.parse();
if (options.src === undefined && options.file === undefined) {
    Log.info("Please specific the source(s) to parse [usage: --file=./results/xxx/urls.txt or --src=http://tvmost.com.hk/20150902-nothing-here]");
    process.exit();
}

var start = function(urls) {
    console.log("Start craling "+urls.length+" urls");
    var contentOptions = {
        cli: true
    }
    if (options.file !== undefined) {
        contentOptions.name = options.file.match(/\/([^\/]*)\/urls.txt/)[1]
    }
    var batchSize = options['batch-size'];
    var total = urls.length;
    var onFinished = function() {
        console.log("DONE")
    }
    var next = function() {
        var batchUrls = urls.splice(0, batchSize);
        var tasks = _.map(batchUrls, function(url) { return detailCrawler.getContent(url, contentOptions) })
        Q.all(tasks).fin(function() {
            console.log("Numer of urls captured: "+(total-urls.length))
            if (urls.length > 0) { next(); } else { onFinished(); }
        })
    }

    console.log("Tota: "+total)
    next();
}

if (options.file !== undefined) {
    var urls = [];
    require('readline')
        .createInterface({ input: require('fs').createReadStream(options.file) })
        .on('line', function (line) { urls.push(line); })
        .on('close', function() {
            start(urls);
        })
} else {
    start(options.src);
}