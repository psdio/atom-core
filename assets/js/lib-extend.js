window.atom = {
    config: !{ JSON.stringify(config) }
};
window.paceOptions = {
    ajax: {
        trackMethods: ["GET", "POST"],
        trackWebSockets: false
    },
    elements: false,
    document: false,
    eventLag: false,
    restartOnPushState: true
};