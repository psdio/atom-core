var indexCrawler = require("./core/index-crawler");
var detailCrawler = require("./core/detail-crawler");
var validator = require("validator");
var commandLineArgs = require("command-line-args");
var sprintf = require("sprintf");
var Log = require("./core/utils/log");
var Toolkit = require("./core/utils/toolkit");
var url = require('url');
var UrlParse = require("./core/modules/url-parse");
var _ = require("underscore");

var cli = commandLineArgs([
    { name: "host", type: String, defaultOption: true },
    { name: "advanced", type: Boolean },
    { name: "regex", type: String, defaultValue: '.*' },
    { name: "name", type: String },
]);
var options = cli.parse();
if (options.host === undefined) {
    Log.info("Please specific the host to parse [ usage: --host=http://tvmost.com.hk ]");
    process.exit();
}

if (options.regex === undefined) {
    Log.info("Please specific the regex for the url of detail page [ usage: --regex= ]");
    process.exit();
}

var advancedMode = !!options.advanced;
var endpoint = Toolkit.resolveUrl(options.host);
var regex = new RegExp(options.regex);

if (!endpoint) {
    Log.info("Invalid endpoint");
    process.exit();
}

indexCrawler.getPossibleUrls(endpoint, {
    endpoint: endpoint,
    advanced: advancedMode,
    complete: function(urls) {
        urls = _.chain(urls)
                .filter(function(url) { 
                    return validator.isURL(url) && regex.test(url); 
                })
                .uniq()
                .value()
        console.log("Available Urls: "+urls.length);
        _writeContent(sprintf("./results/%s", options.name ? options.name : UrlParse(endpoint).host), "urls.txt", urls.sort().join("\n"));
    }
});

function _writeContent(folder, filename, content) {
    Toolkit.makeDirectoryByPath(folder);
    var outputFilename = folder+"/"+filename;
    require('fs').writeFile(outputFilename, content, function(err) {
        if (err) {
            console.log("ERROR");
            console.log(err);
            // Log.error(err);
        } else {
            Log.info("Site feature saved to " + outputFilename);
        }
    }); 
}