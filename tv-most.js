var indexCrawler = require("./core/index-crawler");
var detailCrawler = require("./core/detail-crawler");
var validator = require("validator");
var sprintf = require("sprintf");
var Log = require("./core/Toolkit/log");
var Toolkit = require("./core/Toolkit/toolkit");
var _ = require("underscore");

var endpoint = "http://www.tvmost.com.hk";
var endpoint = "http://listverse.com";

// detailCrawler.getContent("http://www.tvmost.com.hk/201509292303_article_pklopk");
detailCrawler.getContent("http://listverse.com/2015/09/30/10-presidential-kinfolk-who-embarrassed-the-potus/");

// indexCrawler.getPossibleUrls(endpoint, function(urls) {

//     var regex = new RegExp("(.*\/\/(.*\.)?listverse\.com\/\\d+\/\\d+\/\\d+\/.*)");
//     // var regex = new RegExp("(.*\/\/(.*\.)?tvmost\.com\.hk\/\\d+\_article\_.*)");
//     urls = _.filter(urls, function(url) { 
//         return validator.isURL(url) && regex.test(url); 
//     });

//     // console.log(urls);
//     // process.exit();

//     detailCrawler.captureSiteFeatures(urls, function(commonPaths) {
//         saveFeature(endpoint, commonPaths);
//     });
// });

function saveFeature(endpoint, commonPaths) {
    var data = {
        endpoint: endpoint,
        commonPaths: commonPaths
    };

    var parts = require('url').parse(endpoint);
    var dir = sprintf("./results/%s", parts.hostname);
    Toolkit.makeDirectoryByPath(dir);

    var outputFilename = dir+"/feature.json";
    require('fs').writeFile(outputFilename, JSON.stringify(data, null, 4), function(err) {
        if (err) {
            console.log("ERROR");
            console.log(err);
            // Log.error(err);
        } else {
            Log.info("Site feature saved to " + outputFilename);
        }
    }); 
}